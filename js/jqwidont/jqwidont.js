(function ($) {
    $('h1,h2,h3,li,p,.widont').each(function() {
        $(this).html($(this).html().replace(/\s([^\s<]{0,10})\s*$/,'&nbsp;$1'));
    });
})(jQuery);
