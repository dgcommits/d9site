/* add clickable class to make views rows clickable */

(function ($) {
Drupal.behaviors.clickableRowsOnViews = {
  attach: function() {

    jQuery('.clickrow .views-row').each(function() {
      if (jQuery(this).find('a').length) {
        jQuery(this).click(function() {
          window.location = jQuery(this).find('a').attr('href');
          return false;
        });
      }
    });
  }
}
})(jQuery);